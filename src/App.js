import React, { Component } from "react";
// * Custom LESS styles as required
import "./assets/styles/App.less";
import Weather from "./components/Weather/Weather.component";
import WeatherForm from "./components/Weather/Weather-form.component";

const API_KEY = "8c594682e0fbfb41e1d0bcb31c8a7f3b";

class App extends Component {
	/////////////////////////
	// * STATE

	state = {
		temperature: undefined,
		city: undefined,
		humidity: undefined,
		description: undefined,
		icon: undefined,
		error: undefined
	};
	/////////////////////////

	// getDefaultWeather = async e => {
	// 	const api_call = fetch(
	// 		`http://api.openweathermap.org/data/2.5/weather?q=Plovdiv&appid=${API_KEY}&units=metric`
	// 	);
	// 	const data = await api_call.json();

	// 	this.setState({
	// 		temperature: Math.floor(data.main.temp),
	// 		city: data.name,
	// 		condition: data.weather[0].description,
	// 		description: data.weather[0].description,
	// 		icon: `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`,
	// 		error: ""
	// 	});
	// };

	// componentDidMount() {
	// 	this.getDefaultWeather();
	// }

	getWeather = async e => {
		e.preventDefault();
		const city = e.target.elements.city.value;
		const api_call = await fetch(
			`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric`
		);
		const data = await api_call.json();
		if (city) {
			console.log(data);
			this.setState({
				temperature: Math.floor(data.main.temp),
				city: data.name,
				condition: data.weather[0].description,
				description: data.weather[0].description,
				icon: `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`,
				error: ""
			});
		} else {
			this.setState({
				temperature: undefined,
				city: undefined,
				condition: undefined,
				description: undefined,
				icon: undefined,
				error: "Please enter a city."
			});
		}
	};

	render() {
		return (
			<div
				className={`App ${
					this.state.condition === "clear sky" ||
					this.state.condition === "few clouds"
						? "sunny"
						: "cloudy"
				}`}
			>
				<div className="container">
					<WeatherForm getWeather={this.getWeather} />

					<Weather
						temperature={this.state.temperature}
						city={this.state.city}
						description={this.state.description}
						condition={this.state.condition}
						icon={this.state.icon}
						error={this.state.error}
					/>
				</div>
			</div>
		);
	}
}

export default App;
