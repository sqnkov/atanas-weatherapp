import React, { Component } from "react";
import sunnyImage from "../../assets/images/sunny.svg";
import cloudyImage from "../../assets/images/cloudy.svg";

class Weather extends Component {
	render() {
		return (
			<div>
				<div className="weather">
					<div className="weather-degrees">
						{this.props.temperature || `**`}
					</div>
					<img
						className="weather-icon"
						src={this.props.icon}
						alt={this.props.condition}
					/>
				</div>
				<div className="weather-condition">{this.props.condition}</div>
				{this.props.error && <p>{this.props.error}</p>}
				{/* <div className="weather-more">
					<a href="#">More details</a>
				</div> */}
				<div className="weather-image">
					<img
						className="image cloudy"
						src={cloudyImage}
						alt="cloudy"
					/>
					<img className="image sunny" src={sunnyImage} alt="sunny" />
				</div>
			</div>
		);
	}
}

export default Weather;
