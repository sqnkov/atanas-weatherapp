import React, { Component } from "react";

class WeatherForm extends Component {
	clearField = e => {};

	render() {
		return (
			<form className="weather-form" onSubmit={this.props.getWeather}>
				<input
					className="form-input"
					type="text"
					name="city"
					placeholder="enter city"
					onClick={this.clearField}
				/>
				<button className="form-submit">
					<i className="icon icon-zoom"></i>
				</button>
			</form>
		);
	}
}

export default WeatherForm;
